// 推荐博客: https://blog.csdn.net/houyanhua1/article/details/79443987

var fs = require('fs');
var path = require('path');

/**
 * @deprecated 文件夹拷贝到某一个位置
 *
 * @param {String} src
 * @param {String} dest
 * @returns
 */
function CopyDirectory(src, dest) {
  if (!fs.existsSync(src)) {
    return false;
  }
  if (!fs.existsSync(dest)) {
    fs.mkdirSync(dest);
  }
  // 拷贝新的内容进去
  var dirs = fs.readdirSync(src);
  dirs.forEach(function (item) {
    var item_path = path.join(src, item);
    var temp = fs.statSync(item_path);
    if (temp.isFile()) { // 是文件
      fs.copyFileSync(item_path, path.join(dest, item));
    } else if (temp.isDirectory()) { // 是目录
      CopyDirectory(item_path, path.join(dest, item));
    }
  });
}

/**
 * @description 多个目录文件合并到一个目录下
 *
 * @param {Array} dirPathArr  工程目录路径-必需参数
 * @param {String} mergePath 合并后的工程路径-默认参数
 * @example mergeDir(['E:/test/demo1', 'E:/test/demo2'])
 */
function mergeDir(dirPathArr, mergePath) {
  var callback
  if (typeof (arguments[arguments.length - 1]) == 'function') {
    callback = arguments[arguments.length - 1]
  }
  if (!Array.isArray(dirPathArr)) {
    callback({
      code: 500,
      msg: '工程目录必需为数组'
    })
    return
  }
  if (dirPathArr.length < 2) {
    callback({
      code: 500,
      msg: '工程目录数组不能为单个工程目录'
    })
    return
  }
  // 目标路径, 默认放在E盘下
  mergePath = typeof (arguments[1]) == 'string' && mergePath ? mergePath : 'E:/' + 'quickapp_vivo_' + new Date().getTime()
  dirPathArr.forEach(item => {
    CopyDirectory(item, mergePath)
  })
  callback({
    code: 200,
    msg: '合并成功',
    value: mergePath
  })
}

/**
 * @description 文件夹删除
 *
 * @param {String} dir
 */
function DeleteDirectory(dir) {
  if (fs.existsSync(dir) == true) {
    var files = fs.readdirSync(dir);
    files.forEach(function (item) {
      var item_path = path.join(dir, item);
      if (fs.statSync(item_path).isDirectory()) {
        DeleteDirectory(item_path);
      } else {
        fs.unlinkSync(item_path); // 删除文件
      }
    });
    fs.rmdirSync(dir); // 删除文件夹
  }
}

// 3. fs.writeFile  写入文件（会覆盖之前的内容）（文件不存在就创建）  utf8参数可以省略 
// fs.writeFile('123.txt', '你好nodejs 覆盖', 'utf8', function (error) {
//   if (error) {
//     console.log(error);
//     return false;
//   }
//   console.log('写入成功');
// })
// fs.writeFileSync('123.txt', '初始数据')

// 4. fs.appendFile 追加文件  
// fs.appendFile('123.txt', '这是写入的内容\n', function (error) {
//   if (error) {
//     console.log(error);
//     return false;
//   }
//   console.log('写入成功');
// })
// fs.appendFileSync('123.txt', '这是写入的内容\n')


// 5.fs.readFile 读取文件  
// fs.readFile('123.txt', function (error, data) {
//   if (error) {
//     console.log(error);
//     return false;
//   }
//   //console.log(data);  //data是读取的十六进制的数据。  也可以在参数中加入编码格式"utf8"来解决十六进制的问题;
//   console.log(data.toString()); //读取出所有行的信息  
// })
// console.log(fs.readFileSync('123.txt', 'utf-8'))


// 6.fs.readdir 读取目录下第一级内容  把目录下面的文件和文件夹都获取到。  
// fs.readdir('a', function (error, data) {
//   if (error) {
//     console.log(error);
//     return false;
//   }
//   console.log(data); //data是数组类型，包含文件夹以及文件的名字(只有第一级目录内容)。拿到一个文件夹下面的所有目录  
// })



module.exports = mergeDir